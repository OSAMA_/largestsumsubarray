package com.epam.de.dsa;

public class MaxSubArraySum {
    public int maxSumOfSubArray(int[] elements) {
        int currSum=0;
        int maxSum=Integer.MIN_VALUE;

        for(int i=0;i<elements.length;i++){
            currSum += elements[i];
            maxSum = Math.max(currSum,maxSum);
            if(currSum<0) currSum=0;
        }

        return maxSum;
    }
}
