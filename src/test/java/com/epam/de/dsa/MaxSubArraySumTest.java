package com.epam.de.dsa;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaxSubArraySumTest {

    MaxSubArraySum maxSubArraySum;

    @BeforeEach
    void setUp(){
        maxSubArraySum = new MaxSubArraySum();
    }

    @Test
    void maxSumOfSubArrayTest(){
        int[] arr1 = {1, 4, 7, 8, 10, 34, 78};
        int[] arr2 = {-2, -3, -1, -6};
        assertEquals(142,maxSubArraySum.maxSumOfSubArray(arr1));
        assertEquals(-1,maxSubArraySum.maxSumOfSubArray(arr2));
    }

}